import angular from 'angular';
const ngRoute = require('angular-route');
import routing from './main.routes';

export class MainController {

  // timePoints=[];
  /*@ngInject*/
  constructor($http,$scope) {
    this.$http = $http;
    this.$scope = $scope;
    this.timePoints=[];
    this.timerRunning = false;
    this.playBtn=true;
    this.stopBtn=false;
  }

  $onInit() {
    this.$http.get('/api/things')
      .then(response => {
        this.awesomeThings = response.data;
      });

    this.$scope.$on('timer-tick',(event, data) => {
      Object.assign(this,data)
    });

  }

  startTimer() {
    this.playBtn=false;
    this.stopBtn=true;
  }
  stopTimer(){
    this.playBtn=true;
    this.stopBtn=false;
  }
  delete(){
    console.log('delete');
  }
  // checkMoment
  rememberTime(minutes,seconds,millis){
    var millis = this.millis % 1000;
    // let millis = this.millis+30;
    console.log(this.millis,'остаток от дделения на 100:::', millis);
    console.log('minutes:',this.minutes,this.seconds, this.millis);
    let timePoint={minutes:this.minutes, seconds:this.seconds, millis: millis};
    this.timePoints.push(timePoint);
    console.log('array of timePoints:',this.timePoints);
  }
}

export default angular.module('testFullstackApp.main', [ngRoute])
  .config(routing)
  .component('main', {
    template: require('./main.html'),
    controller: MainController
  })
  .name;
